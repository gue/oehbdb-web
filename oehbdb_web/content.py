import oehbdb_web


class Page:

    def __init__(self, path: str, title: str, template: str = 'page.html'):
        self._type = 'page'
        self._path = '/' + path.lstrip('/')
        self._template = template
        self._title = title

    @property
    def type(self) -> str:
        return self._type

    @property
    def path(self) -> str:
        return self._path

    def link_path(self) -> str:
        return oehbdb_web.site_context_get('docroot') + self._path.removesuffix('index.html')

    @property
    def template(self) -> str:
        return self._template

    @property
    def title(self) -> str:
        return self._title

    def jinja_context(self) -> dict:
        ctx = {
            'page': self
        }
        return ctx


class NavigationItem:

    def __init__(self, page: Page, label: str):
        self._page = page
        self._label = label

    @property
    def page(self) -> Page:
        return self._page

    @property
    def label(self) -> str:
        return self._label

    @property
    def href(self) -> str:
        return self._page.link_path()

