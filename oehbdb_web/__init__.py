


_site_context = {
    "docroot": '',
    'html_lang': 'de',
    'site_name': "ÖHBDB",
}


def site_context() -> dict:
    return _site_context


def site_context_get(key: str) -> any:
    return _site_context.get(key, None)


def site_context_set(key: str, value: any) -> None:
    global _site_context
    _site_context[key] = value

