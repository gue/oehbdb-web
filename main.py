import os
import sys

import staticjinja
from staticjinja import Site, Reloader

import oehbdb_web.content


def abs_url(path):
    path = path.ltrim('/')
    return f"/{path}"


def page_url():
    pass


def ctx_nav(current_path):

    nav_links = [
        {
            'name': 'Übersicht',
            'path': '/',
            'active': current_path == '/',
        },
        {
            'name': 'Verbände',
            'path': '/verbaende.html',
            'active': current_path == '/verbaende.html',
        },
        {
            'name': 'Spielorte',
            'path': '/spielorte.html',
            'active': current_path == '/spielort.html',
        },
    ]

    for pos, link in enumerate(nav_links):
        pass

    return [
        {
            'name': 'Übersicht',
            'path': '/',
            'active': current_path == '/',
        },
        {
            'name': 'Verbände',
            'path': '/verbaende.html',
            'active': current_path == '/verbaende.html',
        },
        {
            'name': 'Spielorte',
            'path': '/spielorte.html',
            'active': current_path == '/spielort.html',
        },
    ]


def render_page(site: staticjinja.Site, page: oehbdb_web.content.Page) -> None:
    template = site.get_template(page.template)
    out_file = os.path.join(site.outpath, page.path.lstrip('/'))
    site.render_template(template, page.jinja_context(), out_file)


if __name__ == "__main__":

    docroot = ''
    oehbdb_web.site_context_set('docroot', docroot)
    site = Site.make_site(outpath="output", searchpath=os.getcwd() + "/templates", env_globals=oehbdb_web.site_context())

    index = oehbdb_web.content.Page('/index.html', 'ÖHBDB')
    verbaende = oehbdb_web.content.Page('/verbaende.html', 'Verbände')
    saison = oehbdb_web.content.Page('/saison-2022-23.html', 'Saison 2022/23')
    pages = set((index, verbaende, saison))

    nav_front = oehbdb_web.content.NavigationItem(index, 'Übersicht')
    nav_verbaende = oehbdb_web.content.NavigationItem(verbaende, 'Verbände')
    nav_saison = oehbdb_web.content.NavigationItem(saison, 'Saison')
    nav_items = [nav_front, nav_verbaende, nav_saison]
    site.env.globals.update({'navigation_items': nav_items})

    for page in pages:
        render_page(site, page)
