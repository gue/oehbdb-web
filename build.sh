#!/usr/bin/env sh

if [ ! -d venv ]
then
  echo creating python venv
  python3 -m venv venv
  echo installing requirements.txt
  venv/bin/pip install -r requirements.txt
fi
if [ ! -f venv/bin/python ]
then
  >&2 echo bad python venv
  exit 1
fi

if [ ! -d output ]
then
  mkdir output
else
  rm -r output/*
fi

echo running main.py
venv/bin/python main.py
if [ "$?" != "0" ]
then
  >&2 echo main.py failed
  exit 2
fi
echo copying bootstrap
BOOTSTRAP_DIST=bootstrap-5.0.2-dist
cp $BOOTSTRAP_DIST/js/bootstrap.min.js output/
cp $BOOTSTRAP_DIST/css/bootstrap.min.css output/

