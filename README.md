
# OEHBDB Web

Website generator for oehbdb data.
Based on [staticjinja](https://pypi.org/project/staticjinja/) and [oehbdb](https://codeberg.org/gue/oehbdb).

https://codeberg.org/gue/oehbdb-web


